<?php
/**
 * The header for our theme.
 *
 * @package srp-bel
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="header-nav-wrapper">
	<div class="header-nav">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-9 col-lg-offset-2 col-md-offset-2">
					<button class="toggle">
						<span></span>
					</button>
					<nav class="nav" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<header class="header" role="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-4 col-sm-12 logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php print get_template_directory_uri(); ?>/img/logo.png" alt="logo">
						</a>
					</div>
					<address class="col-md-4 col-sm-6 adr">
						<span><i class="adr-icon"></i>Г.Минск ул.Сурганова, 27, оф.2Н</span>
					</address>
					<div class="col-md-4 col-sm-6 feedback">
						<span class="phone">+375 29 333-52-01</span>
						<button class="primary-btn">Закажите звонок инженера</button>
					</div>
				</div>
				<div class="row">
					<?php if (is_active_sidebar('intro')): ?>
							<?php dynamic_sidebar('intro'); ?>
					<?php endif; ?>
				</div>
				<div class="header-panel">
					<ul>
						<li>соответствие нормам и ГОСТ</li>
						<li>гарантия безопасности</li>
						<li>повышенная вандалоустойчивость</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>

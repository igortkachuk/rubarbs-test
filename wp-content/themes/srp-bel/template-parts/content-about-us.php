<?php
/**
 * Template part for displaying what is this posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package srp-bel
 */

?>

<?php
$count = 1;
$args = array(
    'category_name' => "about-us",
    'post_status' => "publish",
    'posts_per_page' => -1,
    'order' => "ASC"
);
$wp_query = new WP_Query();
$wp_query->query($args);

?>

<ul class="about-us-list">
    <?php if ($wp_query->have_posts()): ?>
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <li class="about-us-list-item">
                <?php the_post_thumbnail(); ?>
                <?php the_content(); ?>
            <li>
        <?php $count++; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</ul>

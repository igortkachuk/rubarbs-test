(function ($) {
    var $navbar = $('.header-nav'),
        height = $navbar.outerHeight();

    /**
     * Handler for the hamburger button
     */
    $('.toggle').on('click', function () {
        $('.nav').slideToggle('300');
    });

    /**
     * Scroll page by anchor
     */
    $('#primary-menu li a').on('click', function () {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - height
        }, 700);
        return false;
    });

    /**
     * Sticky navigation menu
     */
    $(window).scroll(function () {
        if ($(this).scrollTop() > height) {
            $navbar.addClass('sticky');
        } else {
            $navbar.removeClass('sticky');
        }
    });

    /**
     * Waypoints
     */
    $('.about-us-wrapper').waypoint(function () {
        $.each($('.about-us-list-item'), function (i) {
            var that = $(this);
            setTimeout(function () {
                that.addClass('animated slideInUp');
            }, 300 * i);
        });
    }, {
        offset: '70%'
    });
})(jQuery);


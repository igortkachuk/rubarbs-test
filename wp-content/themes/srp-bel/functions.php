<?php
/**
 * srp-bel functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package srp-bel
 */

if ( ! function_exists( 'srp_bel_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function srp_bel_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on srp-bel, use a find and replace
	 * to change 'srp-bel' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'srp-bel', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'srp-bel' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'srp_bel_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'srp_bel_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function srp_bel_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'srp_bel_content_width', 640 );
}
add_action( 'after_setup_theme', 'srp_bel_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function srp_bel_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Intro', 'srp-bel' ),
		'id'            => 'intro',
		'description'   => esc_html__( 'Add widgets here.', 'srp-bel' ),
		'before_widget' => '<section class="widget intro-text">',
		'after_widget'  => '</section>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Clients', 'srp-bel' ),
		'id'            => 'clients',
		'description'   => esc_html__( 'Add widgets here.', 'srp-bel' ),
		'before_widget' => '<li class="clients-list-item">',
		'after_widget'  => '</li>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Services', 'srp-bel' ),
		'id'            => 'services',
		'description'   => esc_html__( 'Add widgets here.', 'srp-bel' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'srp_bel_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function srp_bel_scripts() {
	wp_enqueue_style( 'srp-bel-normalize', get_template_directory_uri () . '/css/normalize.css' );
	wp_enqueue_style( 'srp-bel-bootstrap', get_template_directory_uri () . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'srp-bel-animate', get_template_directory_uri () . '/css/animate.css' );
	wp_enqueue_style( 'srp-bel-main', get_template_directory_uri () . '/css/main.css' );

	wp_enqueue_script( 'srp-bel-waypoints', get_template_directory_uri() . '/js/jquery.waypoints.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'srp-bel-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'srp_bel_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package srp-bel
 */

get_header(); ?>

    <section class="middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="action" class="action-wrapper">
                        <i class="action-icon hidden-sm">Акция</i>
                        <h3>Получите бесплатно планшет с альбомами схем компании</h3>
                        <p>+ Подберём ШРП для вашего конкретного объекта и задачи</p>

                        <?php print do_shortcode('[contact-form-7 id="24" title="Get a prize"]'); ?>
                    </div>
                </div>
            </div>
            <div id="gasification" class="row">
                <div class="col-md-6">
                    <img class="cover" src="<?php print get_template_directory_uri(); ?>/img/gasification.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <h3>МЫ ГОТОВЫ ПРЕДОСТАВИТЬ ВАМ ПОЛНЫЙ КОМПЛЕКС УСЛУГ ПО ГАЗИФИКАЦИИ «ПОД КЛЮЧ»</h3>
                    <h4>Проектирование и строительство газопроводов (от получения первичной разрешительной
                        документации
                        до пуска газа)</h4>
                    <?php if (is_active_sidebar('services')): ?>
                        <?php dynamic_sidebar('services'); ?>
                    <?php endif; ?>
                    <button class="primary-btn">Заказать бесплатный осмотр объекта</button>
                </div>
            </div>
            <div class="row our-clients">
                <div class="col-md-5 col-md-offset-3">
                    <div class="client-title">Наши клиенты</div>
                </div>
                <div class="col-md-12">
                    <?php if (is_active_sidebar('clients')): ?>
                        <ul class="clients-list">
                            <?php dynamic_sidebar('clients'); ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section id="about" class="about-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-us-wrapper">
                        <div class="about-us-title">«ШРп-бел» - ЭТО:</div>

                        <?php get_template_part('template-parts/content', 'about-us'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer();
